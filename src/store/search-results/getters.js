export function isSearchResults (state) {
  return state.searchResults.length > 0
}

export function googleMapsApi (state) {
  return state.googleMapsApi
}
