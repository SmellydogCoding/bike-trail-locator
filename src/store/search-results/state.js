export default function () {
  return {
    searchResults: {},
    searchedYet: false,
    distance: '25',
    location: {},
    zipcode: '',
    googleMapsApi: {}
  }
}
