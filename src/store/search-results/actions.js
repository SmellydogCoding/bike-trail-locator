export function saveSearchResults ({ commit }, searchResults) {
  commit('saveSearchResults', searchResults)
}

export function saveSearchedYet ({ commit }, searchedYet) {
  commit('saveSearchedYet', searchedYet)
}

export function saveDistance ({ commit }, distance) {
  commit('saveDistance', distance)
}

export function saveLocation ({ commit }, location) {
  commit('saveLocation', location)
}

export function saveZipcode ({ commit }, zipcode) {
  commit('saveZipcode', zipcode)
}

export function saveGoogleMapsApi ({ commit }, googleMapsApi) {
  commit('saveGoogleMapsApi', googleMapsApi)
}
