export function saveSearchResults (state, searchResults) {
  state.searchResults = searchResults
}

export function saveSearchedYet (state, searchedYet) {
  state.searchedYet = searchedYet
}

export function saveDistance (state, distance) {
  state.distance = distance
}

export function saveLocation (state, location) {
  state.location = location
}

export function saveZipcode (state, zipcode) {
  state.zipcode = zipcode
}

export function saveGoogleMapsApi (state, googleMapsApi) {
  state.googleMapsApi = googleMapsApi
}
