const defineRoutes = (store) => {
  const routes = [
    {
      path: '/',
      component: () => import('layouts/MainLayout.vue'),
      children: [
        { path: '', name: 'Home', component: () => import('pages/Index.vue') },
        {
          path: 'details',
          name: 'Details',
          component: () => import('pages/Details.vue'),
          beforeEnter: (to, from, next) => {
            const googleMapsApi = store.state.searchResults.googleMapsApi
            if (Object.entries(googleMapsApi).length === 0) {
              next('/')
            } else { next() }
          }
        },
        { path: 'contact', name: 'Contact', component: () => import('pages/Contact.vue') },
        { path: '/:catchAll(.*)*', component: () => import('pages/Error404.vue') }
      ]
    }
  ]

  return routes
}

export default defineRoutes
