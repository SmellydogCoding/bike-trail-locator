import { Screen } from 'quasar'

// set map zoom based on screen size and search radius
export const setZoomLevel = (distance) => {
  const dist = parseInt(distance)
  switch (true) {
    case Screen.lg && dist >= 50:
    case Screen.md && dist >= 25:
    case Screen.sm && dist >= 10 && dist <= 20:
    case Screen.xs && dist >= 10 && dist <= 30: return 8
    case Screen.sm && dist >= 25:
    case Screen.xs && dist >= 40: return 7
    default: return 9
  }
}
